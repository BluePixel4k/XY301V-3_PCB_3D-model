# XY301V-3_PCB_3D-model

A XY301V-3 3D model with PCB footprint and schematic symbol. 
It's a terminal block from the manufacturer Xinya with 3 connectors.

-> Current only the step file. The other files will come later.

# Preview

![preview-3d](https://codeberg.org/BluePixel4k/XY301V-3_PCB_3D-model/raw/branch/main/preview-3d.jpg)

# Datasheet

https://img.waimaoniu.net/479/201802031551203112.pdf
or
https://www.pollin.de/productdownloads/D450857D.PDF

# License

CC0 1.0 Universal (CC0 1.0) Public Domain Dedication